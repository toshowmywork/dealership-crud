package com.dealership.auto.databaseauto.exception;

import lombok.Getter;

import java.util.UUID;

@Getter
public class NotFoundAutoException extends RuntimeException {

    private final UUID id;

    public NotFoundAutoException(UUID id){
        super();
        this.id = id;
    }
}
