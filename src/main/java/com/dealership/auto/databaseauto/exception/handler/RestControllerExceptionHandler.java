package com.dealership.auto.databaseauto.exception.handler;

import com.dealership.auto.databaseauto.exception.AutoInsetedException;
import com.dealership.auto.databaseauto.exception.NotFoundAutoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class RestControllerExceptionHandler {

    @ExceptionHandler(value = {NotFoundAutoException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String notfoundAutoException(NotFoundAutoException ex) {
        log.error("Auto not Found: {}", ex.getId());
        return "Auto not Found: " + ex.getId();
    }

    @ExceptionHandler(value = {AutoInsetedException.class})
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public String insertingExistingAutoException(AutoInsetedException ex) {
        log.error("Trying to insert an existing auto: {}", ex.getMessage(), ex);
        return "Trying to insert an existing auto";
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String resourceNotFoundException(Exception ex) {
        log.error("Internal server error: {}", ex.getMessage(), ex);
        return "Internal server error";
    }

}
