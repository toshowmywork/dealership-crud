package com.dealership.auto.databaseauto.config;

import com.dealership.auto.databaseauto.controller.dto.AutoDto;
import com.dealership.auto.databaseauto.controller.dto.BikeDto;
import com.dealership.auto.databaseauto.controller.dto.BusDto;
import com.dealership.auto.databaseauto.controller.dto.CarDto;
import com.dealership.auto.databaseauto.controller.dto.MotoDto;
import com.dealership.auto.databaseauto.dao.entity.AutoEntity;
import com.dealership.auto.databaseauto.dao.entity.BikeEntity;
import com.dealership.auto.databaseauto.dao.entity.BusEntity;
import com.dealership.auto.databaseauto.dao.entity.CarEntity;
import com.dealership.auto.databaseauto.dao.entity.MotoEntity;
import com.dealership.auto.databaseauto.model.Auto;
import com.dealership.auto.databaseauto.model.Bike;
import com.dealership.auto.databaseauto.model.Bus;
import com.dealership.auto.databaseauto.model.Car;
import com.dealership.auto.databaseauto.model.Moto;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        setAutoMapperConfig(modelMapper);
        return modelMapper;
    }

    private void setAutoMapperConfig(ModelMapper modelMapper) {
        modelMapper.createTypeMap(CarDto.class, Car.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BusDto.class, Bus.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BikeDto.class, Bike.class)
                .include(Auto.class);
        modelMapper.createTypeMap(MotoDto.class, Moto.class)
                .include(Auto.class);

        modelMapper.createTypeMap(Car.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Bus.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Bike.class, CarDto.class)
                .include(AutoDto.class);
        modelMapper.createTypeMap(Moto.class, CarDto.class)
                .include(AutoDto.class);

        modelMapper.createTypeMap(CarEntity.class, Car.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BusEntity.class, Bus.class)
                .include(Auto.class);
        modelMapper.createTypeMap(BikeEntity.class, Bike.class)
                .include(Auto.class);
        modelMapper.createTypeMap(MotoEntity.class, Moto.class)
                .include(Auto.class);

        modelMapper.createTypeMap(Car.class, CarEntity.class)
                .include(AutoEntity.class);
        modelMapper.createTypeMap(Bus.class, BusEntity.class)
                .include(AutoEntity.class);
        modelMapper.createTypeMap(Bike.class, BikeEntity.class)
                .include(AutoEntity.class);
        modelMapper.createTypeMap(Moto.class, MotoEntity.class)
                .include(AutoEntity.class);
    }
}
