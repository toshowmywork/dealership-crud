package com.dealership.auto.databaseauto.dao;

import com.dealership.auto.databaseauto.dao.entity.AutoEntity;
import com.dealership.auto.databaseauto.dao.repository.IAutoRepository;
import com.dealership.auto.databaseauto.exception.AutoInsetedException;
import com.dealership.auto.databaseauto.exception.NotFoundAutoException;
import com.dealership.auto.databaseauto.model.Auto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AutoDao implements IAutoDao {

    private final ModelMapper modelMapper;
    private final IAutoRepository autoRepository;

    public Auto insertAuto(Auto auto){
        AutoEntity autoEntity = addIdToNewAuto(auto);
        AutoEntity autoSaved = insertAuto(autoEntity);
        return modelMapper.map(autoSaved, Auto.class);
    }

    public Auto selectOneAuto(UUID id){
        AutoEntity autoSaved = autoRepository.findById(id)
                                            .orElseThrow(() -> new NotFoundAutoException(id));
        return modelMapper.map(autoSaved, Auto.class);
    }

    public List<Auto> selectAllAuto(Auto auto){
        List<AutoEntity> listAutos = findAutosInDataBase(auto);
        return listAutos.stream().map(gettedAuto -> modelMapper.map(gettedAuto, Auto.class))
                                .collect(Collectors.toList());
    }

    public Auto deleteAuto(UUID id) {
        AutoEntity autoSaved = prepareAutoToDelete(id);
        AutoEntity autoDeleted = insertAuto(autoSaved);
        return modelMapper.map(autoDeleted, Auto.class);
    }

    public Auto updateAuto(Auto autoToUpdate) {
        chekIfAutoExist(autoToUpdate);
        AutoEntity autoEntity = modelMapper.map(autoToUpdate, AutoEntity.class);
        AutoEntity autoUpdated = insertAuto(autoEntity);
        return modelMapper.map(autoUpdated, Auto.class);
    }



    private void chekIfAutoExist(Auto autoToUpdate) {
        UUID id = autoToUpdate.getId();
        autoRepository.findById(id).orElseThrow(() -> new NotFoundAutoException(id));
    }

    private AutoEntity prepareAutoToDelete(UUID id) {
        AutoEntity autoSaved = autoRepository.findById(id)
                .orElseThrow(() -> new NotFoundAutoException(id));
        autoSaved.setDeleted(true);
        return autoSaved;
    }


    private AutoEntity addIdToNewAuto(Auto auto) {
        AutoEntity autoEntity = modelMapper.map(auto, AutoEntity.class);
        autoEntity.setId(UUID.randomUUID());
        return autoEntity;
    }

    private AutoEntity insertAuto(AutoEntity autoEntity) {
        try{
            return autoRepository.save(autoEntity);
        } catch (DataIntegrityViolationException divex){
            throw new AutoInsetedException(divex);
        }
    }

    private List<AutoEntity> findAutosInDataBase(Auto auto) {
        AutoEntity autoEntity = modelMapper.map(auto, AutoEntity.class);
        return autoRepository.findAll(Example.of(autoEntity));
    }

}
