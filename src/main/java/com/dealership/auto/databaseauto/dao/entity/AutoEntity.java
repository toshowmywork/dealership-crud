package com.dealership.auto.databaseauto.dao.entity;

import com.dealership.auto.databaseauto.util.AutoEnum;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        visible = true,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CarEntity.class, name = "CAR"),
        @JsonSubTypes.Type(value = MotoEntity.class, name = "MOTO"),
        @JsonSubTypes.Type(value = BusEntity.class, name = "BUS"),
        @JsonSubTypes.Type(value = BikeEntity.class, name = "BIKE")
})
@Getter
@Setter
@ToString
@EqualsAndHashCode
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"type", "brand", "model"})
})
public class AutoEntity {

    @Id
    private UUID id;

    private AutoEnum type;
    private String brand;
    private String model;

    @Builder.Default
    private Boolean deleted = false;

}
