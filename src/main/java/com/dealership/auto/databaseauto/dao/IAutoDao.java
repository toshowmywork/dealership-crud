package com.dealership.auto.databaseauto.dao;

import com.dealership.auto.databaseauto.model.Auto;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

public interface IAutoDao {

    Auto insertAuto(@RequestBody Auto auto);
    Auto selectOneAuto(UUID id);
    List<Auto> selectAllAuto(Auto auto);
    Auto deleteAuto(UUID id);
    Auto updateAuto(Auto autoToUpdate);
}
