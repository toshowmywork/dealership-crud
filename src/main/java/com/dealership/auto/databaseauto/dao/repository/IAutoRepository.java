package com.dealership.auto.databaseauto.dao.repository;

import com.dealership.auto.databaseauto.dao.entity.AutoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface IAutoRepository extends JpaRepository<AutoEntity, UUID> {
    List<AutoEntity> findByDeleted(boolean b);
}
