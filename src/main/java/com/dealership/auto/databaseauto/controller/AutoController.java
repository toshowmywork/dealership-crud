package com.dealership.auto.databaseauto.controller;

import com.dealership.auto.databaseauto.controller.dto.AutoDto;
import com.dealership.auto.databaseauto.dao.IAutoDao;
import com.dealership.auto.databaseauto.model.Auto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static java.util.Optional.ofNullable;

@RestController
@RequestMapping("auto")
@RequiredArgsConstructor
public class AutoController {

    private final ModelMapper modelMapper;
    private final IAutoDao autoDao;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Auto insert(@RequestBody @Valid AutoDto autoDto){
        Auto autoToInsert = modelMapper.map(autoDto, Auto.class);
        return autoDao.insertAuto(autoToInsert);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Auto selectOne(@PathVariable UUID id){
        return autoDao.selectOneAuto(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Auto> selectList(@ModelAttribute AutoDto autoDto){
        Auto autoToInsert = mapAutoDtoToAutoModel(autoDto);
        return autoDao.selectAllAuto(autoToInsert);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Auto update(@PathVariable UUID id, @RequestBody AutoDto autoDto){
        Auto autoToUpdate = modelMapper.map(autoDto, Auto.class);
        autoToUpdate.setId(id);
        return autoDao.updateAuto(autoToUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Auto delete(@PathVariable UUID id){
        return autoDao.deleteAuto(id);
    }



    private Auto mapAutoDtoToAutoModel(AutoDto autoDto) {
        return ofNullable(autoDto).map(auto -> modelMapper.map(auto, Auto.class))
                .orElse(null);
    }

}
