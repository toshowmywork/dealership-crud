package com.dealership.auto.databaseauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class DatabaseAutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseAutoApplication.class, args);
	}

}
