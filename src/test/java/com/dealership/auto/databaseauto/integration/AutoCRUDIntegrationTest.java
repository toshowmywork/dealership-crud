package com.dealership.auto.databaseauto.integration;

import com.dealership.auto.databaseauto.dao.entity.AutoEntity;
import com.dealership.auto.databaseauto.dao.entity.BikeEntity;
import com.dealership.auto.databaseauto.dao.entity.BusEntity;
import com.dealership.auto.databaseauto.dao.entity.CarEntity;
import com.dealership.auto.databaseauto.dao.repository.IAutoRepository;
import com.dealership.auto.databaseauto.model.Auto;
import com.dealership.auto.databaseauto.model.Bus;
import com.dealership.auto.databaseauto.model.Car;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static com.dealership.auto.databaseauto.util.SourceReader.getResourceAsString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AutoCRUDIntegrationTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    IAutoRepository autoRepository;

    @BeforeEach
    void setUp(){
        autoRepository.deleteAll();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "integration/insert/car-to-insert.json",
            "integration/insert/bike-to-insert.json",
            "integration/insert/bus-to-insert.json",
            "integration/insert/moto-to-insert.json"
    })
    void insertOneAuto(String pathAuto) throws Exception {
        String autoAsString = getResourceAsString(pathAuto);

        mockMvc.perform(post("/auto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(autoAsString))
                .andExpect(status().isCreated());

        AutoEntity expectedAuto = objectMapper.readValue(autoAsString, AutoEntity.class);
        List<AutoEntity> insertedAuto = autoRepository.findAll();

        assertThat(insertedAuto.size()).isEqualTo(1);
        assertThat(insertedAuto.get(0))
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedAuto);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "integration/insert/car-to-insert.json",
            "integration/insert/bike-to-insert.json",
            "integration/insert/bus-to-insert.json",
            "integration/insert/moto-to-insert.json"
    })
    void insertOtherAuto(String pathAuto) throws Exception {

        String autoAsString = getResourceAsString(pathAuto);
        AutoEntity autoInseertedBefore = objectMapper.readValue(autoAsString, AutoEntity.class);
        autoInseertedBefore.setId(UUID.randomUUID());
        autoInseertedBefore.setBrand(autoInseertedBefore.getBrand() + "1");
        autoRepository.save(autoInseertedBefore);

        mockMvc.perform(post("/auto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(autoAsString))
                .andExpect(status().isCreated());

        AutoEntity expectedAuto = objectMapper.readValue(autoAsString, AutoEntity.class);
        List<AutoEntity> insertedAuto = autoRepository.findAll();

        assertThat(insertedAuto.size()).isEqualTo(2);
        assertThat(insertedAuto)
                .usingElementComparatorIgnoringFields("id")
                .contains(expectedAuto);
        assertThat(insertedAuto).contains(autoInseertedBefore);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "integration/insert/car-to-insert.json",
            "integration/insert/bike-to-insert.json",
            "integration/insert/bus-to-insert.json",
            "integration/insert/moto-to-insert.json"
    })
    void insertExistingAuto(String pathAuto) throws Exception {

        String autoAsString = getResourceAsString(pathAuto);
        JsonNode jsonNode = objectMapper.readTree(autoAsString);
        String autoinsertedString = "{\"type\": " + jsonNode.at("/type") + ", " +
                                    "\"brand\": " + jsonNode.at("/brand") + ", " +
                                    "\"model\": " + jsonNode.at("/model") + "}";
        AutoEntity insertedAuto = objectMapper.readValue(autoinsertedString, AutoEntity.class);
        insertedAuto.setId(UUID.randomUUID());
        autoRepository.save(insertedAuto);

        mockMvc.perform(post("/auto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(autoAsString))
                .andExpect(status().isConflict())
                .andExpect(content().string("Trying to insert an existing auto"));
    }


    @ParameterizedTest
    @ValueSource(strings = {
            "integration/select/one/car-to-select.json",
            "integration/select/one/bike-to-select.json",
            "integration/select/one/bus-to-select.json",
            "integration/select/one/moto-to-select.json"
    })
    void selectOneAuto(String pathAuto) throws Exception {
        String autoAsString = getResourceAsString(pathAuto);
        UUID randomUUID = UUID.randomUUID();
        AutoEntity insertedAuto = objectMapper.readValue(autoAsString, AutoEntity.class);
        insertedAuto.setId(randomUUID);
        autoRepository.save(insertedAuto);
        Auto expectedAuto = objectMapper.readValue(autoAsString, Auto.class);
        expectedAuto.setId(randomUUID);
        String expectedAutoString = objectMapper.writeValueAsString(expectedAuto);

        mockMvc.perform(get("/auto/"+insertedAuto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedAutoString));
    }

    @Test
    void selectNoAuto() throws Exception {
        UUID randomUUID = UUID.randomUUID();
        mockMvc.perform(get("/auto/" + randomUUID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void selectAllAuto() throws Exception {
        String carAsString = getResourceAsString("integration/select/all/car-to-select.json");
        CarEntity insertedCar = objectMapper.readValue(carAsString, CarEntity.class);
        UUID randomUUIDCar = UUID.randomUUID();
        insertedCar.setId(randomUUIDCar);
        autoRepository.save(insertedCar);
        Car expectedCar = objectMapper.readValue(carAsString, Car.class);
        expectedCar.setId(randomUUIDCar);
        String expectedCarAsString = objectMapper.writeValueAsString(expectedCar);

        String busAsString = getResourceAsString("integration/select/all/bus-to-select.json");
        BusEntity insertedBus = objectMapper.readValue(busAsString, BusEntity.class);
        UUID randomUUIDBus = UUID.randomUUID();
        insertedBus.setId(randomUUIDBus);
        autoRepository.save(insertedBus);
        Bus expectedBus = objectMapper.readValue(busAsString, Bus.class);
        expectedBus.setId(randomUUIDBus);
        String expectedBusAsString = objectMapper.writeValueAsString(expectedBus);

        String expectedListAuto = "[" + expectedCarAsString + ", " + expectedBusAsString + "]";

        mockMvc.perform(get("/auto")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedListAuto));

    }

    @Test
    void selectExampleAuto() throws Exception {
        String carAsString = getResourceAsString("integration/select/all/car-to-select-example.json");
        CarEntity insertedCar = objectMapper.readValue(carAsString, CarEntity.class);
        UUID randomUUIDCar = UUID.randomUUID();
        insertedCar.setId(randomUUIDCar);
        autoRepository.save(insertedCar);
        Car expectedCar = objectMapper.readValue(carAsString, Car.class);
        expectedCar.setId(randomUUIDCar);
        String expectedCarString = objectMapper.writeValueAsString(expectedCar);

        String busAsString = getResourceAsString("integration/select/all/bus-to-select-example.json");
        BusEntity insertedBus = objectMapper.readValue(busAsString, BusEntity.class);
        UUID randomUUIDBus = UUID.randomUUID();
        insertedBus.setId(randomUUIDBus);
        autoRepository.save(insertedBus);
        Bus expectedBus = objectMapper.readValue(busAsString, Bus.class);
        expectedBus.setId(randomUUIDBus);
        String expectedBusString = objectMapper.writeValueAsString(expectedBus);

        String pathBike = "integration/select/all/bike-to-select-example.json";
        String bikeAsString = getResourceAsString(pathBike);
        BikeEntity insertedBike = objectMapper.readValue(bikeAsString, BikeEntity.class);
        insertedBike.setId(UUID.randomUUID());
        autoRepository.save(insertedBike);

        String expectedListAuto = "[" + expectedCarString + ", " + expectedBusString + "]";

        mockMvc.perform(get("/auto?brand=Mercedes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedListAuto));

    }

    @Test
    void updateAuto() throws Exception {

        String autoAsString = getResourceAsString("integration/delete/car-to-delete.json");
        AutoEntity autoInsertedBefore = objectMapper.readValue(autoAsString, AutoEntity.class);
        autoInsertedBefore.setId(UUID.randomUUID());
        autoRepository.save(autoInsertedBefore);

        String autoAsString2 = getResourceAsString("integration/delete/bus-to-delete.json");
        AutoEntity autoInsertedBefore2 = objectMapper.readValue(autoAsString2, AutoEntity.class);
        UUID randomUUID = UUID.randomUUID();
        autoInsertedBefore2.setId(randomUUID);
        autoInsertedBefore2.setBrand("Nueva Marca");
        autoRepository.save(autoInsertedBefore2);

        mockMvc.perform(put("/auto/"+randomUUID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(autoAsString2))
                .andExpect(status().isOk());

        AutoEntity autoUpdated = objectMapper.readValue(autoAsString2, AutoEntity.class);
        autoUpdated.setId(randomUUID);
        List<AutoEntity> insertedAuto = autoRepository.findAll();

        assertThat(insertedAuto.size()).isEqualTo(2);
        assertThat(insertedAuto).contains(autoInsertedBefore)
                                .contains(autoUpdated);
    }

    @Test
    void updateNonExistingAuto() throws Exception {

        String autoAsString = getResourceAsString("integration/delete/car-to-delete.json");
        AutoEntity autoInsertedBefore = objectMapper.readValue(autoAsString, AutoEntity.class);
        autoInsertedBefore.setId(UUID.randomUUID());
        autoRepository.save(autoInsertedBefore);

        mockMvc.perform(put("/auto/"+UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(autoAsString))
                .andExpect(status().isNotFound());

    }

    @Test
    void deleteAuto() throws Exception {

        String autoAsString = getResourceAsString("integration/delete/car-to-delete.json");
        AutoEntity autoInsertedBefore = objectMapper.readValue(autoAsString, AutoEntity.class);
        autoInsertedBefore.setId(UUID.randomUUID());
        autoRepository.save(autoInsertedBefore);

        String autoAsString2 = getResourceAsString("integration/delete/bus-to-delete.json");
        AutoEntity autoInsertedBefore2 = objectMapper.readValue(autoAsString2, AutoEntity.class);
        UUID randomUUID = UUID.randomUUID();
        autoInsertedBefore2.setId(randomUUID);
        autoRepository.save(autoInsertedBefore2);
        autoInsertedBefore2.setDeleted(true);

        mockMvc.perform(delete("/auto/"+randomUUID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<AutoEntity> insertedAuto = autoRepository.findAll();

        assertThat(insertedAuto.size()).isEqualTo(2);
        assertThat(insertedAuto).contains(autoInsertedBefore)
                                .contains(autoInsertedBefore2);
    }

}
